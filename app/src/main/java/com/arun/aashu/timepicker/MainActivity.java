package com.arun.aashu.timepicker;

import android.app.TimePickerDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TimePicker;

import java.util.Calendar;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        ((EditText)findViewById(R.id.time)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Calendar ca=Calendar.getInstance();

                new TimePickerDialog(MainActivity.this,time,
                        ca.get(Calendar.HOUR_OF_DAY),
                        ca.get(Calendar.MINUTE),true).show();


            }
        });


    }

    TimePickerDialog.OnTimeSetListener time=new TimePickerDialog.OnTimeSetListener() {
        @Override
        public void onTimeSet(TimePicker timePicker, int hourOfday, int minute) {

            ((EditText)findViewById(R.id.time)).setText(hourOfday+" : "+minute);


        }
    };
}
